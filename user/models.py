import uuid
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Company(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return "%s" % (self.name)

class Department(models.Model):
    name = models.CharField(max_length=100)
    # CASCADE means if company is deleted, the correspoding department will be deleted as well
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return "%s" % (self.name)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE,primary_key=True)
    full_name = models.CharField(max_length=100, blank=True)
    # CASCADE means if company is deleted, the correspoding profile will be deleted as well
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
    )
    department = models.ForeignKey(
        Department,
        default=None,
        on_delete=models.SET_DEFAULT,
        blank=True,
        null=True,
    )
    is_dept_admin = models.BooleanField(default=False)
    is_company_admin = models.BooleanField(default=False)
    is_superior = models.BooleanField(default=False)

    def __str__(self):
        return "Profile - %s" % (self.full_name)


# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Profile.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.profile.save()