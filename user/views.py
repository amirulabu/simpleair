from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, login, authenticate, update_session_auth_hash
from django.contrib import messages
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from user.models import Profile
from user.forms import ProfileForm, SetPasswordFormExtra, AuthenticationFormExtra

def index(request):
    return render(request, 'user/front.html')

def change_password(request):
    if request.method == 'POST':
        form = SetPasswordFormExtra(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('index')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = SetPasswordFormExtra(request.user)
    return render(request, 'user/change_password.html', {'form': form})