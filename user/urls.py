from django.urls import path,include
from django.contrib.auth import views as auth_views
from . import views
from django.http import HttpResponseRedirect
from django.shortcuts import reverse
from user.forms import AuthenticationFormExtra, PasswordResetFormExtra, SetPasswordFormExtra

urlpatterns = [
    path('', views.index, name="index"),
    
    path('login/', auth_views.LoginView.as_view(
        template_name='user/login.html',
        authentication_form=AuthenticationFormExtra,
    ), name='login'),
    
    path('user/logout/', auth_views.LogoutView.as_view(), name="logout"),
    
    path('user/password-change/', views.change_password, name="password_change"),
    
    path('user/password-reset/', auth_views.PasswordResetView.as_view(
        template_name='user/reset.html',
        form_class=PasswordResetFormExtra,
    ), name='password_reset'),

    path('user/password-reset/done/',auth_views.PasswordResetView.as_view(
        template_name='user/reset.html',
        form_class=PasswordResetFormExtra,
        extra_context={
            'alert_success':'Password reset successful, We\'ve e-mailed you instructions for setting your password to the e-mail address you submitted. You should be receiving it shortly.'
        },
    ), name='password_reset_done'),
    
    path('user/password-reset/reset/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(
        template_name='user/change_password.html',
        form_class=SetPasswordFormExtra,
    ),name='password_reset_confirm'),
    
    # path('user/password-reset/', auth_views.PasswordResetCompleteView.as_view(
    #     template_name='user/reset.html',
    # ), name='password_reset_complete'),

    path('user/password-reset/login', auth_views.LoginView.as_view(
        template_name='user/login.html',
        authentication_form=AuthenticationFormExtra,
        extra_context={'alert_success':'Password reset completed, please login with your new password'},
    ), name='password_reset_complete'),
]