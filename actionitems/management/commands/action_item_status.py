from actionitems.models import Template, ActionItem, Update, Reminder
from user.models import Department, Company, Profile
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import timedelta

from django.core.management.base import BaseCommand



class Command(BaseCommand):
    def handle(self, *args, **options):
        # change action item status from open to overdue according to due date

        action_item_open = ActionItem.objects.filter(status="OPEN")
        arr = []
        for a in action_item_open:
            if a.is_past_due:
                arr.append(a.id)
        
        action_item_open.filter(id__in=arr).update(status="OVERDUE")

        # action_item_overdue = ActionItem.objects.filter(status="OVERDUE")

        
