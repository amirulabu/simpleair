from actionitems.models import Template, ActionItem, Update, Reminder
from user.models import Department, Company, Profile
from django.contrib.auth.models import User
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils import timezone
from datetime import timedelta
from django.db.models import Q
from django.conf import settings

from django.core.management.base import BaseCommand



class Command(BaseCommand):
    def handle(self, *args, **options):
        # reminder triggered for open action item
        action_item = ActionItem.objects.filter(status="OPEN")
        for ai in action_item:
            reminder = Reminder.objects.filter(action_item=ai).filter(
                reminder_date__date=timezone.now().date())
            if reminder:
                email_to = ai.assignee.values_list("email", flat=True)
                email_cc = []
                for email in ai.owner.values_list("email", flat=True):
                    email_cc.append
                for email in ai.initiator.values_list("email", flat=True):
                    email_cc.append

                context = {
                    "action_item": ai,
                    "full_url": settings.FULL_BASE_URL,
                }
                subject_txt = render_to_string(
                    'actionitems/email/subject_reminder.txt', context=context)
                content_txt = render_to_string(
                    'actionitems/email/reminder.txt', context=context)
                content_html = render_to_string(
                    'actionitems/email/reminder.html', context=context)

                email = EmailMultiAlternatives(
                    subject_txt,
                    content_txt,
                    'ActionItemDB <app-noreply@actionitemdb.com>',
                    to=email_to,
                    cc=email_cc,
                )
                email.attach_alternative(content_html, "text/html")
                sent = email.send()
                if sent == 1:
                    r = reminder[0]
                    r.is_triggered = True
                    r.save()

        action_item = ActionItem.objects.filter(status="OVERDUE")
        for ai in action_item:
            reminders = Reminder.objects.filter(action_item=ai).order_by('-reminder_date')
            reminder = reminders[0] if reminders else False
            if reminder and (timezone.now() - reminder.reminder_date).days >= 4:
                email_to = ai.assignee.values_list("email", flat=True)
                email_cc = []
                for email in ai.owner.values_list("email", flat=True):
                    email_cc.append
                for email in ai.initiator.values_list("email", flat=True):
                    email_cc.append

                context = {
                    "action_item": ai,
                    "full_url": settings.FULL_BASE_URL,
                }
                subject_txt = render_to_string('actionitems/email/subject_reminder.txt', context=context)
                content_txt = render_to_string('actionitems/email/reminder.txt', context=context)
                content_html =  render_to_string('actionitems/email/reminder.html', context=context)

                email = EmailMultiAlternatives(
                    subject_txt, 
                    content_txt,
                    'ActionItemDB <app-noreply@actionitemdb.com>',
                    to=email_to,
                    cc=email_cc,
                )
                email.attach_alternative(content_html, "text/html")
                sent = email.send()
                if sent == 1:
                    Reminder.objects.create(
                        action_item=ai,
                        reminder_date=timezone.now(),
                        is_triggered=True,
                    )

        # if 1hb every month, get all open action item, put into summary for each person.
        if timezone.now().date().day == 1:
            users = User.objects.filter(
                Q(assignee__status="OPEN") | Q(owner__status="OPEN") | Q(initiator__status="OPEN") |
                Q(assignee__status="OVERDUE") | Q(owner__status="OVERDUE") | Q(initiator__status="OVERDUE")
            ).distinct()

            for user in users:

                email_to = [user.email]
                action_items = ActionItem.objects.filter(
                    Q(owner=user) | Q(assignee=user) | Q(initiator=user)
                )
                context = {
                    "user":user,
                    "action_items": action_items,
                    "full_url": settings.FULL_BASE_URL,
                }
                subject_txt = "Monthly summary for your action items"
                content_txt = render_to_string('actionitems/email/summary.txt', context=context)
                content_html =  render_to_string('actionitems/email/summary.html', context=context)
                
                email = EmailMultiAlternatives(
                    subject_txt,
                    content_txt,
                    'ActionItemDB <app-noreply@actionitemdb.com>',
                    to=email_to,
                )
                email.attach_alternative(content_html, "text/html")
                email.send()

        
