from django.apps import AppConfig


class ActionitemsConfig(AppConfig):
    name = 'actionitems'
