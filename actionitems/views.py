from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from actionitems.models import Template, ActionItem, Update, Reminder
from django.db.models import Q
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.safestring import SafeString
from django.core.serializers import serialize
from django.forms import modelformset_factory
from .forms import ActionItemForm, UpdateForm, ReminderForm, ReminderFormSetHelper
from .functions import add_reminders
import json, csv

@login_required
def index(request):
    company = request.user.profile.company
    department = request.user.profile.department
    templates = Template.objects.filter(company=company).select_related('action_item')
    action_items = ActionItem.objects.filter(template__in=templates)
    if department:
        action_items = action_items.filter(department=department)
    action_items_a = ActionItem.objects.filter(Q(owner=request.user)|Q(assignee=request.user)).distinct()
    action_items_a_ov = action_items_a.filter(status="OVERDUE").count()
    action_items_a_open = action_items_a.filter(status="OPEN").count()
    action_items_a_prop = action_items_a.filter(status="PROPOSE-CLOSE").count()
    action_items_a_comp = action_items_a.filter(status="COMPLETED").count()
    action_items_i = ActionItem.objects.filter(Q(initiator=request.user)).distinct()
    action_items_i_ov = action_items_i.filter(status="OVERDUE").count()
    action_items_i_open = action_items_i.filter(status="OPEN").count()
    action_items_i_prop = action_items_i.filter(status="PROPOSE-CLOSE").count()
    action_items_i_comp = action_items_i.filter(status="COMPLETED").count()


    context = {
        "company": company,
        "department": department,
        "action_items": action_items,
        "action_items_a": action_items_a,
        "action_items_a_ov": action_items_a_ov,
        "action_items_a_open": action_items_a_open,
        "action_items_a_prop": action_items_a_prop,
        "action_items_a_comp": action_items_a_comp,
        "action_items_i": action_items_i,
        "action_items_i_ov": action_items_i_ov,
        "action_items_i_open": action_items_i_open,
        "action_items_i_prop": action_items_i_prop,
        "action_items_i_comp": action_items_i_comp,
    }
    return render(request, 'actionitems/index.html', context)

@login_required
def list_by_status(request, as_a, status):
    list_status = ['OPEN','OVERDUE','PROPOSE-CLOSE','COMPLETED']
    if as_a == "owner":
        action_items = ActionItem.objects.filter(Q(owner=request.user)|Q(assignee=request.user)).distinct()
    elif as_a == "initiator":
        action_items = ActionItem.objects.filter(Q(initiator=request.user)).distinct()
    else:
        raise Http404

    if status.upper() in list_status:
        action_items = action_items.filter(status=status.upper())
    else:
        raise Http404

    context = {
        "action_items": action_items,
        "as_a": as_a,
        "status": status,
    }
    return render(request, 'actionitems/list.html', context)


@login_required
def action_item(request, action_item_id):
    action_item = ActionItem.objects.get(id=action_item_id)
    updates = Update.objects.filter(action_item=action_item)
    reminder = Reminder.objects.filter(action_item=action_item)#.distinct('reminder_date')
    creator = User.objects.filter(creator__id=action_item_id)
    initiator = User.objects.filter(initiator__id=action_item_id)
    owner = User.objects.filter(owner__id=action_item_id)
    assignee = User.objects.filter(assignee__id=action_item_id)
    people_can_edit = creator.union(creator, initiator)
    peoples = creator.union(creator, initiator, owner, assignee)
    people_can_propose_close = creator.union(owner,assignee)
    status_show_close_btn = ['OPEN','OVERDUE','PROPOSE-CLOSE',]
    status_show_propose_close_btn = ['OPEN','OVERDUE',]

    if request.user in people_can_edit or request.user.is_superuser:
        can_edit = True
    else:
        can_edit = False

    if request.user in people_can_propose_close or request.user.is_superuser:
        can_propose_close = True 
    else:
        can_propose_close = False
    
    if request.user in peoples or request.user.is_superuser:
        if request.method == 'POST':
            form = UpdateForm(data=request.POST)
            if form.is_valid():
                update = form.save(commit=False)
                update.user = request.user
                update.action_item = action_item
                update.save()
                return HttpResponseRedirect(reverse('action_item', args=[action_item.id]))
        else:
            form = UpdateForm()
    else:
        form = False
    context = {
        "action_item": action_item,
        "updates": updates,
        "form": form,
        "reminder": reminder,
        "can_edit": can_edit,
        "can_propose_close": can_propose_close,
        "status_show_close_btn": status_show_close_btn,
        "status_show_propose_close_btn": status_show_propose_close_btn,

    }
    return render(request, 'actionitems/action_item.html', context)

@login_required
def action_item_create(request, template_id=None):
    company = request.user.profile.company
    # import pdb; pdb.set_trace()
    if(template_id==None):
        templates = Template.objects.filter(company=company)
        template = None
        schema = json.dumps(None)
        options = json.dumps(None)
    else:
        try:
            templates = None
            template = Template.objects.filter(company=company).get(id=template_id)
            schema = json.dumps(template.schema)
            options = json.dumps(template.options)
        except:
            raise Http404
    if request.method == 'POST':
        form = ActionItemForm(data=request.POST)
        if form.is_valid():
            action_item = form.save(commit=False)
            if template.schema['properties']:
                for k,v in template.schema['properties'].items():
                    # DANGER! TODO sanitize the data from POST
                    # well i tried.
                    action_item.details[k] = json.JSONEncoder().encode(request.POST[k])[1:-1]
            action_item.creator = request.user
            action_item.template = template
            action_item.save()
            form.save_m2m()
            add_reminders(action_item)
            return HttpResponseRedirect(reverse('action_item', args=[action_item.id]))
    else:
        form = ActionItemForm(company=company, initial={'initiator': request.user.id })
    context = {
        "templates": templates,
        "form": form,
        "template": template,
        "schema": schema,
        "options": options,
    }
    return render(request, 'actionitems/action_item_create.html', context)

@login_required
def action_item_edit(request,action_item_id):
    try:
        action_item = ActionItem.objects.get(id=action_item_id)
        company = request.user.profile.company
        template = action_item.template
        schema = json.dumps(template.schema)
        options = json.dumps(template.options)

        creator = User.objects.filter(creator__id=action_item_id)
        initiator = User.objects.filter(initiator__id=action_item_id)
        people_can_edit = creator.union(creator, initiator)
    except:
        raise Http404

    if request.user in people_can_edit or request.user.is_superuser:
        pass
    else:
        raise Http404
    
    if request.method == 'POST':
        form = ActionItemForm(instance=action_item,data=request.POST)
        # import pdb; pdb.set_trace()
        if form.is_valid():
            action_item = form.save(commit=False)
            if template.schema['properties']:
                for k,v in template.schema['properties'].items():
                    # DANGER! TODO sanitize the data from POST
                    # well i tried.
                    action_item.details[k] = json.JSONEncoder().encode(request.POST[k])[1:-1]
            else:
                action_item.details[None]= None
            action_item.creator = request.user
            action_item.template = template
            action_item.save()
            form.save_m2m()
            return HttpResponseRedirect(reverse('action_item', args=[action_item.id]))
    else:
        form = ActionItemForm(company=company,instance=action_item)

    context = {
        "form": form,
        "template": template,
        "schema": schema,
        "options": options,
    }
    return render(request, 'actionitems/action_item_create.html', context)

@login_required
def action_item_close(request,action_item_id):
    try:
        action_item = ActionItem.objects.get(id=action_item_id)
        creator = User.objects.filter(creator__id=action_item_id)
        initiator = User.objects.filter(initiator__id=action_item_id)
        people_can_edit = creator.union(creator, initiator)
        reminder = Reminder.objects.filter(action_item=action_item)
    except:
        raise Http404
    
    if request.user in people_can_edit or request.user.is_superuser:
        pass
    else:
        raise Http404

    if request.method == 'POST':
        for r in reminder:
            if not r.is_past_due:
                r.delete()
        action_item.status = "COMPLETED"
        action_item.save()
        return HttpResponseRedirect(reverse('action_item', args=[action_item.id]))
    context = { "action_item": action_item }
    return render(request, 'actionitems/action_item_close.html', context)

@login_required
def action_item_propose_close(request,action_item_id):
    try:
        action_item = ActionItem.objects.get(id=action_item_id)
        owner = User.objects.filter(owner__id=action_item_id)
        assignee = User.objects.filter(assignee__id=action_item_id)
        people_propose_close = owner.union(assignee)
    except:
        raise Http404

    
    if request.user in people_propose_close or request.user.is_superuser:
        pass
    else:
        raise Http404

    if request.method == 'POST':
        action_item.status = "PROPOSE-CLOSE"
        action_item.save()
        return HttpResponseRedirect(reverse('action_item', args=[action_item.id]))
    context = { "action_item": action_item }
    return render(request, 'actionitems/action_item_propose_close.html', context)

@login_required
def update_delete(request, action_item_id, update_id):
    try:
        update = Update.objects.get(id=update_id)
    except:
        raise Http404
    if request.user != update.user:
        raise Http404

    if request.method == 'POST':
        update.delete()
        return HttpResponseRedirect(reverse('action_item', args=[action_item_id]))
    context = { "update": update, "action_item_id": action_item_id }
    return render(request, 'actionitems/update_delete.html', context)

@login_required
def reminder_add(request, action_item_id):
    try:
        action_item = ActionItem.objects.get(id=action_item_id)
        reminder = Reminder.objects.filter(action_item=action_item)
    except:
        raise Http404
    ReminderFormSet = modelformset_factory(Reminder, fields=('reminder_date',), can_delete=True)

    if request.method == 'POST':
        formset = ReminderFormSet(queryset=reminder,data=request.POST)
        helper = ReminderFormSetHelper()
        if formset.is_valid():
            form = formset.save(commit=False)
            for obj in formset.deleted_objects:
                obj.delete()

            for reminder in form:
                reminder.action_item = action_item
                reminder.save()
            # delete all reminders with same reminder_date
            current_reminders = Reminder.objects.filter(action_item=action_item)
            for r in current_reminders.values_list('reminder_date', flat=True).distinct():
                current_reminders.filter(id__in=current_reminders.filter(reminder_date=r).values_list('id', flat=True)[1:]).delete()

            return HttpResponseRedirect(reverse('action_item', args=[action_item_id]))
    else:
        formset = ReminderFormSet(queryset=reminder)
        helper = ReminderFormSetHelper()

    context = { "action_item": action_item, "formset": formset, "helper": helper }
    return render(request, 'actionitems/reminder_add.html', context)

    
@login_required
def template_index(request):
    company = request.user.profile.company
    templates = Template.objects.filter(company=company)
    context = { "templates": templates }
    return render(request, 'actionitems/templates.html', context)

@login_required
def template_csv_dl(request, template_id):
    company = request.user.profile.company
    template = Template.objects.get(id=template_id)

    if template.company.name != company.name:
        raise Http404

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="action_item_upload.csv"'

    writer = csv.writer(response)
    
    row = [
        'title','finding','smart_action_item','due_date','department',
        'email_initiator_1','email_initiator_2','email_initiator_3','email_initiator_4','email_initiator_5',
        'email_owner_1','email_owner_2','email_owner_3','email_owner_4','email_owner_5',
        'email_assignee_1','email_assignee_2','email_assignee_3','email_assignee_4','email_assignee_5',
    ]
    if template.schema['properties']:
        for k in template.schema['properties'].keys():
            row.append(k)

    writer.writerow(row)
    
    return response

@login_required
def template_csv_ul(request, template_id):
    pass



