{% extends "actionitems/email/base.txt" %}
{% block content %}
Hi {{ user.profile.full_name }},

Summary for all your open and overdue action items on ActionItemDB.

{% for ai in action_items|dictsort:"published_date" %}

{{ forloop.counter }}. {{ ai.title }} ({{ ai.get_status_display }}): {{ full_url }}{% url "action_item" ai.id %}

{% endfor %}

{% endblock content %}