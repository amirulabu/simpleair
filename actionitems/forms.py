from django import forms
from django.contrib.postgres.forms import SplitArrayField,SimpleArrayField
from django.urls import reverse
from django.contrib.auth.models import User
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Button, Submit, HTML, Field, Div, Hidden
from user.models import Company, Department
from .models import ActionItem, Update, Reminder
from django.forms import modelformset_factory

class ActionItemForm(forms.ModelForm):

    class Meta:
        model = ActionItem
        fields = [
            'title', 
            'finding', 
            'smart_action_item',
            'classification',
            # 'status',
            'due_date',
            'initiator',
            'owner',
            'assignee',
            'department',
        ]

    
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company', None)
        super(ActionItemForm, self).__init__(*args, **kwargs)
        if company:
            self.fields['initiator'].queryset = User.objects.filter(profile__company__id=company.id)
            self.fields['owner'].queryset = User.objects.filter(profile__company__id=company.id)
            self.fields['assignee'].queryset = User.objects.filter(profile__company__id=company.id)
            self.fields['department'].queryset = Department.objects.filter(company__id=company.id)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))
        self.helper.layout = Layout(
            Field('title'),
            Field('finding'),
            Field('smart_action_item'),
            Field('due_date'),
            Field('classification'),
            Field('status'),
            Field('department'),
            Field('initiator', css_class='selectpicker'),
            Field('owner', css_class='selectpicker'),
            Field('assignee', css_class='selectpicker'),
            HTML(
                "<div id=\"details\"></div>"
            )
        )
    
    title = forms.CharField(
        label = "Title",
        required = True,
    )

    finding = forms.CharField(
        widget = forms.Textarea(
            attrs={'rows':4, 'cols':15}),
        required=False,
        help_text = "Detail issues/area of concern",
    )

    smart_action_item = forms.CharField(
        widget = forms.Textarea(
            attrs={'rows':4, 'cols':15}),
        required=False,
        help_text = "Detail information on the action item using SMART",
    )

    class MyModelMultipleChoiceField(forms.ModelMultipleChoiceField):
        def label_from_instance(self, obj):
            return "%s (%s)" % (obj.profile.full_name, obj.username)

    
    initiator = MyModelMultipleChoiceField(
        widget = forms.SelectMultiple(
            attrs = {"data-live-search":"true"},
        ),
        queryset = User.objects.all(),
    )

    owner = MyModelMultipleChoiceField(
        widget = forms.SelectMultiple(
            attrs = {"data-live-search":"true"},
        ),
        required=False,
        queryset = User.objects.all(),
    )

    assignee = MyModelMultipleChoiceField(
        widget = forms.SelectMultiple(
            attrs = {"data-live-search":"true"},
        ),
        queryset = User.objects.all(),
    )

    department = forms.ModelChoiceField(
        queryset = Department.objects.all(),
    )
    
    due_date = forms.DateTimeField(
        label = "Due Date",
        required = True,
    )

    # status = forms.ChoiceField(
    #     choices = ActionItem.STATUS_CHOICES,
    # )

    classification = forms.ChoiceField(
        choices = ActionItem.CLASSIFICATION_CHOICES,
    )

class ActionItemStatusForm(forms.ModelForm):

    class Meta:
        model = ActionItem
        fields = [
            'status',
        ]

    status = forms.ChoiceField(
        choices = ActionItem.STATUS_CHOICES,
    )

class UpdateForm(forms.ModelForm):

    class Meta: 
        model = Update
        fields = ['text']

    def __init__(self, *args, **kwargs):
        super(UpdateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Submit'))
        self.helper.layout = Layout(
            Field('text'),
            HTML("<p> by {{ user.profile.full_name }}</p>"),
        )

    text = forms.CharField(
        label = '',
        widget=forms.Textarea(
            attrs={'rows':4, 'cols':15, 'placeholder':'Add your update/remarks'}),
    )

class ReminderForm(forms.ModelForm):

    class Meta: 
        model = Reminder
        fields = ['reminder_date']

    reminder_date = forms.DateTimeField(
        label = '',
        widget = forms.DateTimeInput(
            attrs={"placeholder":"Set date for reminder"}
        )
    )

    # action_item = forms.ModelChoiceField(
    #     queryset = ActionItem.objects.all(),
    # )


class ReminderFormSetHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super(ReminderFormSetHelper, self).__init__(*args, **kwargs)
        self.form_method = 'post'
        # self.add_input(Button('minus', '- row', css_class='btn btn-warning'))
        self.add_input(Button('add', '+ row', css_class='btn btn-default'))
        self.form_show_labels = False
        self.add_input(Submit('submit', 'Submit', css_class='pull-right', onclick="goBack()"))
        self.layout = Layout(
            Div(
                Div(
                    Field('reminder_date'),
                    HTML("<input type=hidden name='form-{{ forloop.counter0 }}-DELETE' value=''>"),
                    css_class="col-sm-10",
                ),
                Div(
                    HTML("<button name='{{ forloop.counter0 }}-DELETE' class='close pull-left' type='button'> &times; </button>"),
                    css_class="col-sm-2",
                ),
                css_class="row datepicker-set",
            )
        )
        self.render_required_fields = True
