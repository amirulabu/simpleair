from django.utils import timezone
from django.conf import settings
from actionitems.models import Reminder
import datetime 

def add_reminders(action_item):
    """ 
    takes action_item object and add reminders with default reminder_date.
    """
    reminders = []

    # send a couple reminders a few days before due_date - according to settings.SEND_REMINDER_BEFORE
    for day in settings.SEND_REMINDER_BEFORE:
        if day < (action_item.due_date - timezone.now()).days:
            reminders.append(Reminder( action_item=action_item, reminder_date=(action_item.due_date - datetime.timedelta(days=day))) ) 

    Reminder.objects.bulk_create(reminders)