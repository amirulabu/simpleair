from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='action_item_index'),
    path('list/<str:as_a>/<str:status>', views.list_by_status, name='action_item_list'),
    path('<int:action_item_id>/', views.action_item, name='action_item'),
    path('create', views.action_item_create, name='action_item_create_select'),
    path('create/<int:template_id>', views.action_item_create, name='action_item_create'),
    path('<int:action_item_id>/edit', views.action_item_edit, name='action_item_edit'),
    path('<int:action_item_id>/close', views.action_item_close, name='action_item_close'), # one button to close action item with confirmaton and delete all related reminder
    path('<int:action_item_id>/propose-close', views.action_item_propose_close, name='action_item_propose_close'), # one button to propose close, with conformation and add remarks
    # path('<int:action_item_id>/change-due-date', views.action_item_change_due_date, name='action_item_change_date'), # one button to change due date
    path('<int:action_item_id>/reminder/add', views.reminder_add, name='reminder_add'),
    path('<int:action_item_id>/delete/<int:update_id>', views.update_delete, name='update_delete'),
    # search by template name - for searching details that is not in the main model
    path('template/', views.template_index, name='template_index'),
    path('template/<int:template_id>/download-csv', views.template_csv_dl, name='template_csv_dl'),
    path('template/<int:template_id>/upload-csv', views.template_csv_ul, name='template_csv_ul'),

]