from django.contrib import admin
from .models import Template,Update,ActionItem,Reminder

admin.site.register(Template)
admin.site.register(ActionItem)
admin.site.register(Update)
admin.site.register(Reminder)