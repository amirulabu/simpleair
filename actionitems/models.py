from django.db import models
from django.contrib.postgres.fields import JSONField, ArrayField
from django.contrib.auth.models import User
from user.models import Department, Company
from datetime import date
from django.utils import timezone
import json

class Template(models.Model):
    """ Template is the template for details required and default settings for the action item(Item) """
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=300, blank=True)
    # CASCADE means if the user is deleted, all the template under it will also be deleted
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
    )
    # schema and options will rely heavily on alpacajs
    schema = JSONField()
    options = JSONField()
    # other settings
    settings = JSONField(default=dict())

    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
    )

    @property
    def get_json_data(self):
        return json.dumps(self.data)

    def __str__(self):
        return self.name

def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]

class ActionItem(models.Model):
    """ Item model is the action item, that follows the template it is related to """
    
    title = models.CharField(max_length=100)
    finding = models.TextField(blank=True)
    smart_action_item = models.TextField(blank=True)

    # CASCADE means if the template is deleted, all the action items under it will also be deleted
    template = models.ForeignKey(
        Template,
        on_delete=models.CASCADE,
    )

    CLASSIFICATION_CHOICES = (
        ('L','Low/ OFI'),
        ('M','Medium'),
        ('H','High'),
        ('VH','Very High/ Serious/ NCR'),
    )
    classification = models.CharField(max_length=2,choices=CLASSIFICATION_CHOICES,default='M')

    # date that the item is first published
    published_date = models.DateTimeField(auto_now_add=True)
    # date that shows the last modified date
    last_modified_date = models.DateTimeField(auto_now=True)
    # due date is the agreed date to complete the action item
    due_date = models.DateTimeField()
    @property
    def is_past_due(self):
        if timezone.now().date() > self.due_date.date():
            return True
        return False
    
    # a JSON of all the custom details for the form
    details = JSONField(default=dict())

    # auto put at the person who created the item
    creator = models.ForeignKey(
        User,
        on_delete=models.SET(get_sentinel_user),
        related_name='creator'
    )

    # a group of people who is interested of the action item
    initiator = models.ManyToManyField(
        User,
        related_name='initiator'
    )

    # a group of people who owns the action item
    owner = models.ManyToManyField(
        User,
        related_name='owner',
        blank=True,
    )

    # a group of people who is assigned the action item
    assignee = models.ManyToManyField(
        User,
        related_name='assignee',
    )

    department = models.ForeignKey(
        Department,
        on_delete=models.CASCADE,
    )

    # make a button if the assignee or owner wants to propose to complete, just push a button with warning
    STATUS_CHOICES = (
        ('OPEN', 'Open'),
        ('OVERDUE', 'Overdue'),
        ('PROPOSE-CLOSE', 'Proposed to close'),
        ('COMPLETED','Completed'),
    )
    status = models.CharField(max_length=20,choices=STATUS_CHOICES, default="OPEN")

    def __str__(self):
        return "%s: %s" % (self.title, self.id)

class Update(models.Model):
    """ This is a section in Item where the action item assignee, owner, and creator can put remarks/progress updates"""
    # if the User is deleted, it will create or use the 'deleted' username
    user = models.ForeignKey(
        User,
        on_delete=models.SET(get_sentinel_user),
    )
    # CASCADE means if item is deleted, the correspoding update will be deleted as well
    action_item = models.ForeignKey(
        ActionItem,
        on_delete=models.CASCADE,
    )
    text = models.TextField()
    published_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s: %s" % (self.user, self.text)

class Reminder(models.Model):
    """ This is a section in Item where the reminder email should be triggered for an Item"""
    # CASCADE means if item is deleted, the correspoding reminder will be deleted as well
    action_item = models.ForeignKey(
        ActionItem,
        on_delete=models.CASCADE,
    )
    reminder_date = models.DateTimeField()
    is_triggered = models.BooleanField(default=False)

    @property
    def is_past_due(self):
        if timezone.now().date() > self.reminder_date.date():
            return True
        return False

    def __str__(self):
        return "(%s) %s: %s" % (self.id, self.reminder_date, self.action_item.title)